# == Schema Information
#
# Table name: high_fives
#
#  id         :integer          not null, primary key
#  card_id    :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  micro_id   :integer
#  link_id    :integer
#

class HighFive < ActiveRecord::Base
  attr_accessible :resource_id, :resource_type, :user_id

  belongs_to :resource, :polymorphic => true
  belongs_to :user
end
