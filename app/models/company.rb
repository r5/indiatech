class Company < ActiveRecord::Base
  attr_accessible :description, :name, :website

  has_many :company_user_profiles, :dependent => :destroy
  has_many :users, :through => :company_user_profile
end
