class CompanyUserProfile < ActiveRecord::Base
  attr_accessible :company_id, :location,:company_name, :title, :summary, :technologies_clone, :user_id, :work_from, :work_till, :working_currently, :technology_list

  belongs_to :user
  belongs_to :company
  acts_as_ordered_taggable_on :technologies


  before_create :create_company

  default_scope order: 'company_user_profiles.working_currently desc, company_user_profiles.work_till DESC'

  def formatted_work_till
  	self.working_currently ? "present" :  Time.zone.at(CompanyUserProfile.last.attributes["work_till"]).strftime("%m %Y")
  end
 private
  def create_company
  	self.build_company(name: self.company_name)
  end
end
