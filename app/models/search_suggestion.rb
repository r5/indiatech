class SearchSuggestion
  def self.terms_for_tags(prefix)
    terms = Redis.current.zrevrange "search-suggestions:#{prefix.downcase}", 0, 9
    terms.map{|t| {:name => t}}
  end
  
  def self.index_user_tags
    User.tag_counts_on(:skills).find_each do |tag|
      index_term(tag.name)
    end
  end
  
  def self.index_term(term)
    1.upto(term.length-1) do |n|
      prefix = term[0, n]
      Redis.current.zincrby "search-suggestions:#{prefix.downcase}", 1, term.downcase
    end
  end
end