# == Schema Information
#
# Table name: social_links
#
#  id         :integer          not null, primary key
#  link       :string(255)
#  link_type  :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SocialLink < ActiveRecord::Base
  attr_accessible :link, :link_type, :user_id
  belongs_to :user
end
