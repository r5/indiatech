# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  repo_url    :string(255)
#  site_url    :string(255)
#  status      :string(255)
#  from        :datetime
#  to          :datetime
#  user_id     :integer
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Project < ActiveRecord::Base
  attr_accessible :description,:photo_ids, :from, :name, :repo_url, :technology_list, :site_url, :status, :to, :user_id

  belongs_to :user
  acts_as_ordered_taggable_on :technologies
  has_many :photos, :as => :attachable, :dependent => :destroy

  scope :recent, order("created_at desc").limit(3)
end
