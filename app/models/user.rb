# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(128)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_name             :string(255)
#  last_name              :string(255)
#  bio                    :text
#  title                  :string(255)
#  company_id             :integer
#  location               :string(255)
#  teaser                 :string(255)
#  status                 :integer          default(0)
#  username               :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  job_availability       :integer
#  freelancer             :boolean
#

class User < ActiveRecord::Base

  include Redis::Objects
  include Gravtastic
  gravtastic :secure => false

  # list :activity_pool
  list :followers_pool
  list :followed_pool
  counter :followers_counter
  counter :followed_counter
  counter :cards_counter
  # TODO: define counter for card

  AVAIL = {0 => 'Not Available', 1 => 'Full Time', 2 => 'Part Time'}

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :company_user_profiles, :dependent => :destroy
  has_many :companies, :through => :company_user_profile
  has_many :schools
  has_many :authentications, :dependent => :destroy 
  has_many :social_links, :dependent => :destroy 
  has_many :cards, :dependent => :destroy
  has_many :high_fives
  has_many :links
  has_many :link_categories
  has_many :projects
  has_many :micros
  has_many :relationships, foreign_key: "follower_id", dependent: :destroy

  has_many :followed_users, through: :relationships, source: :followed

  has_many :reverse_relationships, foreign_key: "followed_id",
                                   class_name:  "Relationship",
                                   dependent:   :destroy

  has_many :followers, through: :reverse_relationships, source: :follower

  has_many :activities

  has_many :notifications, :dependent => :destroy

  attr_accessor :login
  validates :username, :uniqueness => true, :presence => true
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :authentication_keys => [:login]

  # Setup accessible (or protected) attributes for your model

  attr_accessible :email, :password, :login, :password_confirmation, :remember_me, :first_name, :last_name, :bio, :title, :username, :company_id, :location, :teaser, :social_links_attributes,:projects_attributes, :skill_list, :software_list, :freelancer, :job_availability, :company_user_profiles_attributes, :languages, :schools_attributes
  # attr_accessible :title, :body
  accepts_nested_attributes_for :social_links, :reject_if => lambda { |a| a[:link].blank? }, :allow_destroy => true 
  accepts_nested_attributes_for :company_user_profiles, :reject_if => lambda { |cup| cup[:company_name].blank? }, :allow_destroy => true 
  accepts_nested_attributes_for :schools, :reject_if => lambda { |s| s[:name].blank? }, :allow_destroy => true 
  accepts_nested_attributes_for :projects, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
  acts_as_ordered_taggable_on :skills
  # acts_as_ordered_taggable_on :availables

  # acts_as_ordered_taggable_on :softwares

  # has_attached_file :avatar,:path => ":rails_root/public/system/:attachment/:username/:style/:filename",
  #     :url => "/system/:attachment/:username/:style/:filename", :styles => { :medium => "290x290>", :big => "439x439#" }

	def self.find_first_by_auth_conditions(warden_conditions)
	  conditions = warden_conditions.dup
	  if login = conditions.delete(:login)
	    where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
	  else
	    where(conditions).first
	  end
	end

  def recent_activities_oj(user_ids)
    Oj.dump(Activity.where("user_id in (?)", user_ids).limit(10).includes([:user, :trackable => :high_fives]).map{|pa| self.activity_oj_dump(pa)}, :mode => :compat)
  end

  def page_activities_oj(user_ids, page, per_page, type=nil)
    if type
      feed = Activity.paginate(:per_page => per_page, :page => page).where("user_id in (?) && trackable_type = ?", user_ids, type).includes([:user, :trackable => :high_fives])
    else
      feed = Activity.paginate(:per_page => per_page, :page => page).where("user_id in (?)", user_ids).includes([:user, :trackable => :high_fives])
    end
    [Oj.dump(feed.map{|pa| self.activity_oj_dump(pa)}, :mode => :compat), feed.total_pages]
  end

  def activity_oj_dump(activity)
    replies_count = activity.trackable.replies.count if activity.trackable.class.to_s == "Micro"
    content = activity.trackable_type == "Micro" ? activity.trackable.try(:content) : activity.trackable.try(:description) 
    return {
      :id => activity.id,
      :trackable_type => activity.trackable_type,
      :hours_ago => activity.created_at,
      :title => activity.title,
      :trackable => {
        :id => activity.trackable.id,
        :content => content,
        :url => activity.trackable.slug_path,
        :high_fives_length => activity.trackable.high_fives.length,
        :high_fived => activity.trackable.high_fives.map(&:user_id).include?(self.id),  
        :view_count => activity.trackable.view_count,
        :replies_count => replies_count
      },
      :user => {
        :name => activity.user.first_name,
        :profile_url => "/" + activity.user.username,
        :email => activity.user.email,
        :username => activity.user.username,
        :id => activity.user.id
      }
    }
  end

  def followers
    User.where(:id => self.followers_pool.values)
  end

  def followed_users
    User.where(:id => self.followed_pool.values)
  end

  def notify(key, actor=nil, resource=nil)
    self.notifications.create(:key => key, :resource => resource, :actor => actor)
  end

  def following?(other_user)
    relationships.find_by_followed_id(other_user.id)
  end

  def follow!(other_user)
    relationships.create!(followed_id: other_user.id)
    add_to_follow_redis_pool(other_user)
    other_user.notify("user.followed", self)
  end

  def add_to_follow_redis_pool(other_user)
    other_user.followers_pool << self.id
    other_user.followers_counter.increment
    self.followed_pool << other_user.id
    self.followed_counter.increment
  end

  def increament_card_counter
    self.cards_counter.increment
  end

  def decrement_card_counter
    self.cards_counter.decrement
  end

  def unfollow!(other_user)
    relationships.find_by_followed_id(other_user.id).destroy
    remove_from_followed_redis_pool(other_user)
  end

  def remove_from_followed_redis_pool(other_user)
    other_user.followers_pool.delete(self.id)
    self.followed_pool.delete(other_user.id)
    other_user.followers_counter.decrement
    self.followed_counter.decrement
  end

  def self.featured
    # User.where(:featured => true)
    User.last
  end

  def format_username
    "@" + self.username
  end

  def full_name
    if first_name && last_name
      first_name + " " + last_name
    else
      username
    end
  end

  def self.top_eight
    User.limit(12)
  end
end
