class Photo < ActiveRecord::Base
  belongs_to :attachable, :polymorphic => true

  attr_accessible :attachment
  
  has_attached_file :attachment,
                    :styles => {
                      :medium => { :geometry => "300x200#", :processors => nil }
                    }
                    
  validates_attachment_presence :attachment
  validates_attachment_content_type :attachment, :content_type => ['image/jpeg', 'image/png', 'image/gif', 'image/pjpeg', 'image/x-png', 'application/octet', 'application/pdf'], 
                                                  :message => ' is not an image/pdf'
  
  def medium
    attachment.url(:medium)
  end
end