class Activity < ActiveRecord::Base

	attr_accessible :key, :user, :trackable, :recipient 
	# Define polymorphic association to the parent
  belongs_to :trackable, :polymorphic => true
  # Define ownership to a resource responsible for this activity
  belongs_to :user
  # Define ownership to a resource targeted by this activity
  belongs_to :recipient, :polymorphic => true

	default_scope order: 'activities.created_at DESC'

  # after_create :update_redis_user_activity_pool

	def text(params = {})
    # TODO: some helper for key transformation for two supported formats
    k = key.split('.')
    k.unshift('activity') if k.first != 'activity'
    k = k.join('.')

    I18n.t(k, {})
  end

	def title
		self.text().gsub(self.trackable_type.downcase, "<a href='/#{self.trackable_type.downcase}s/#{self.trackable.slug}'>#{self.trackable_type.downcase}</a>")
	end

  # def update_redis_user_activity_pool
  #   user = self.user
  #   user.followers.find_each do |follower|
  #     follower.activity_pool << self.id
  #   end
  # end
	
end
