# == Schema Information
#
# Table name: authentications
#
#  id           :integer          not null, primary key
#  uid          :string(255)
#  provider     :integer
#  access_token :string(255)
#  secret       :string(255)
#  image        :string(255)
#  profile      :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :integer
#

class Authentication < ActiveRecord::Base
  attr_accessible :access_token, :image, :profile, :provider, :secret, :uid

  PROVIDER = {'linkedin' => 1, 'github' => 2}
end
