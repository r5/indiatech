# == Schema Information
#
# Table name: link_categories
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LinkCategory < ActiveRecord::Base
  attr_accessible :title, :user_id
  validates :title, :presence => true
  has_many :links
  belongs_to :user
end
