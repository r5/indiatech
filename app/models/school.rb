class School < ActiveRecord::Base
  attr_accessible :degree, :fileds, :from, :name, :to, :user_id

  belongs_to :user

  default_scope order: 'schools.to DESC'

  def fields
  	self.fileds
  end
end
