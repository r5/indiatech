# == Schema Information
#
# Table name: micros
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  content     :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  in_reply_to :integer
#

class Micro < ActiveRecord::Base
  attr_accessible :content, :user_id, :in_reply_to
  validates :content, :length => { :maximum => 140, :minimum => 5 }
  validates :user_id, :presence => true

  has_many :high_fives, :class_name => HighFive, :as => :resource, :dependent => :destroy

  belongs_to :user

  has_many :replies, :class_name => 'Micro',
                   :foreign_key => "in_reply_to",
                   :inverse_of => :replied_post
  belongs_to :replied_post, :class_name => 'Micro',
                          :foreign_key => "in_reply_to",
                          :inverse_of => :replies

  has_many :activities, :as => :trackable


  

  default_scope order: 'micros.created_at DESC'

  after_create :create_activity_feed

  before_validation :generate_slug


  def to_param
    slug
  end

  def slug_path
    "/micros/#{self.slug}"
  end

  def generate_slug
    self.slug ||= content.parameterize.truncate(100, :omission  => '')
  end

  def self.from_users_followed_by(user)
    followed_user_ids = "SELECT followed_id FROM relationships
                         WHERE follower_id = :user_id"
    where("user_id IN (#{followed_user_ids}) OR user_id = :user_id", 
          user_id: user.id)
  end

private

  def create_activity_feed
    if self.in_reply_to
      self.activities.create({key: 'micro.replied', user: self.user, recipient: self.replied_post, :trackable => self})
    else
      self.activities.create({key: 'micro.created', user: self.user, :trackable => self})
    end
  end
end
