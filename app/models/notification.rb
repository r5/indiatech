class Notification < ActiveRecord::Base
  attr_accessible :closed_at, :key, :position, :viewed_at,  :actor, :user, :resource

  scope :unread, -> { where viewed_at: nil }
  default_scope order: 'notifications.created_at DESC'

  belongs_to :user
  belongs_to :actor, class_name: "User"
  belongs_to :resource, :polymorphic => true

  def text(params = {})
    # TODO: some helper for key transformation for two supported formats
    k = key.split('.')
    k.unshift('notification') if k.first != 'notification'
    k = k.join('.')

    (I18n.t(k, {}) + "<a href='#{self.resource.slug_path}'> #{self.resource_type.downcase} </a>").html_safe
  end
end
