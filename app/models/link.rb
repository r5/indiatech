# == Schema Information
#
# Table name: links
#
#  id               :integer          not null, primary key
#  title            :string(255)
#  description      :text
#  user_id          :integer
#  link_category_id :integer
#  image_url        :string(255)
#  vote_ups         :integer
#  link             :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Link < ActiveRecord::Base
  attr_accessible :description,:link, :image_url, :link_category_id, :title, :user_id, :vote_ups, :tag_list
  belongs_to :user
  belongs_to :link_category
  has_many :high_fives, :class_name => HighFive, :as => :resource, :dependent => :destroy
  validates :link, :presence => true, :url => true
  validates :title, :presence => true


  acts_as_ordered_taggable_on :tags
  default_scope order: 'links.created_at DESC'
  scope :recent, order("created_at desc").limit(6)

  after_create :create_activity_feed
  has_many :activities, :as => :trackable

  validates :slug, :uniqueness => true, :presence => true

  before_validation :generate_slug


  def to_param
    slug
  end

  def generate_slug
    self.slug ||= title.parameterize
  end

  def slug_path
    "/links/#{self.slug}"
  end
  
private
  def create_activity_feed
    self.activities.create({key: 'link.created', user: self.user})
  end
end
