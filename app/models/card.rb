  # == Schema Information
#
# Table name: cards
#
#  id          :integer          not null, primary key
#  description :text
#  view_count  :integer
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Card < ActiveRecord::Base
  attr_accessible :description, :user_id, :view_count
  belongs_to :user
  has_many :high_fives,:class_name => HighFive, :as => :resource, :dependent => :destroy

  has_many :photos, :as => :attachable, :dependent => :destroy

  default_scope order: 'cards.created_at DESC'

  after_create :create_activity_feed

  validates :slug, :uniqueness => true, :presence => true

  before_validation :generate_slug

  has_many :activities, :as => :trackable


  def to_param
    slug
  end

  def generate_slug
    self.slug ||= description.parameterize.truncate(100, :omission  => '')
  end

  def slug_path
    "/cards/#{self.slug}"
  end

private
  def create_activity_feed
    self.activities.create({key: 'card.created', user: self.user})
    self.user.increament_card_counter
  end

end
