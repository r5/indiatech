class SearchSuggestions
  def initialize(app)
    @app = app
  end
  
  def call(env)
    if env["PATH_INFO"] == "/search_suggestions"
      # TODO: lets inspect action for adding more suggestion, like autocomplete
      request = Rack::Request.new(env)
      terms = SearchSuggestion.terms_for_tags(request.params["q"])
      [200, {"Content-Type" => "appication/json"}, [terms.to_json]]
    else
      @app.call(env)
    end
  end
end