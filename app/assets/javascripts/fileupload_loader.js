//=require jquery-fileupload/basic


jQuery(document).ready(function($) {
	$('#fileupload').fileupload({
		dataType: 'script',
	  acceptFileTypes: /(\.|\/)(gif|jpe?g|png|GIF|JPE?G|PNG)$/,
	  add: function(e, data){
	  	$(".photo_container.active").addClass("processing");
	  	data.submit();
	  	$("#fileuploadModal").trigger("reveal:close");
	  },
	  done: function(e, data){
	  }
	});

	$(".photo_container.remove").live("click", function(){

	});

	$(".photo_container.add").live("click",function(){
		$(this).addClass("active")
		$("#fileuploadModal").reveal({
			closed: function(){
				if(!$(".photo_container.add.active").hasClass("processing"));{
					$(".photo_container.add.active").removeClass("active")
				}
			}
		});
		return false;
	});
});