jQuery(document).ready(function($) {
	
	// Signin Modal Reveal
	$("#header-sign-in, .sign-in-rev").click(function(){
		$("#signinModal").find(".s-alert").remove();
		$("#signinModal").reveal();
		return false;
	});

	// Signup Modal Reveal
	$("#header-sign-up, .sign-up-rev").click(function(){
		$("#signupModal").find(".s-alert").remove();
		$("#signupModal").reveal();
		return false;
	});

	$("a.action-post").click(function(){
		var $modal = $($(this).data("modal-id"));
		$modal.find(".s-alert").remove();
		$modal.find('form')[0].reset();
		$modal.reveal();
		return false;
	});

	$(".reveal-signin-modal").live("click", function(){
		$("#signinModal .alert-box").remove();
		$("#signinModal").prepend("<div class='alert-box s-alert'>"+ $(this).data('alert-msg') +"<a href='' class='close'>&times;</a></div>")
		$("#signinModal").reveal();
		return false;
	});
});
