// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require underscore-min
//= require gravtastic
//= require timeago
//= require_tree .


function remove_fields(link) {  
    $(link).prev("input[type=hidden]").val("1");  
    $(link).closest(".fields").hide();  
}  
  
function add_fields(link, association, content) {  
    var new_id = new Date().getTime();  
    var regexp = new RegExp("new_" + association, "g"); 
    content = $(content.replace(regexp, new_id)) 
    $(link).prev().after(content);
    // content.find(".tokenize").tokenInput("/search_suggestions", {theme: "facebook"}); 

}
$(function() {

	$("abbr.timeago").timeago();

	$("textarea.micro_reply").focus(function() {
	  if($(this).val().trim() == ""){
	  	$(this).val($(this).attr("placeholder").replace("Reply to ", ""));
	  	$(this).attr("rows", 4)
	  	$(this).parents("form").find(".button").removeClass("hide")
	  }
	}).blur(function() {
	 	if($(this).attr("placeholder").replace("Reply to ", "") == $(this).val()){
	  	$(this).attr("rows", 1);
	  	$(this).val("")
	  	$(this).parents("form").find(".button").addClass("hide")
	  }
	});
	
});
