//= require feeds
// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
jQuery(document).ready(function($) {
	// Lazy load dashboard stuff
	//Only for dashboard page here===================================================
	if(window.location.pathname == "/dashboard"){
		$.get('/lazy_dashboard', function(data){
			var feed_temp = _.template($("#feed-under-template").html());
			$("#recent-feed-dashboard").append(feed_temp({activities: data}));
			$("abbr.timeago").timeago();
		});
	}
	//Only for dashboard page ends here===================================================

	//Only for feed page here===================================================
	if(window.location.pathname == "/feed"){
		var feed_temp = _.template($("#feed-under-template").html());
		$("#feed-dashboard").append(feed_temp({activities: feeds}));
		$("abbr.timeago").timeago();
	}
	//Only for feed page ends here ===================================================

	// Only for company edit page ============================================================
	$("input.working_currently").on("change", function(){
		if($(this).is(":checked")){
			$(this).parents(".fields").find(".work_till select").hide();
			$(this).parents(".fields").find(".work_till span").show();
		}else{
			$(this).parents(".fields").find(".work_till select").show();
			$(this).parents(".fields").find(".work_till span").hide();
		}	
	}).trigger("change");
	// Only for company edit page ends here ============================================================
});
