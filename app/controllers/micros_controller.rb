class MicrosController < ApplicationController

	before_filter :authenticate_user!, :only => [:create, :update]
	before_filter :find_micro, :only => [:show]

	def create
		@micro = Micro.new(params[:micro])
		if @micro.save
			@micro.replied_post.user.notify("micro.replied", @micro.user, @micro) if @micro.in_reply_to
		end
	end


	def show
		if @micro
			@user = @micro.user
		else

		end
	end

private

	def find_micro
		@micro = Micro.where(:slug => params[:id]).includes(:replies => [:high_fives]).first
	end
end

