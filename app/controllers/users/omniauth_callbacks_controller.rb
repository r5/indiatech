class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

	before_filter :set_or_create_current_user

	def linkedin
		render "/home/index"
	end

	def github
		render "/home/index"
	end

	private

	def set_or_create_current_user
		omniauth = request.env["omniauth.auth"]
		authentication = Authentication.find_by_provider_and_uid(Authentication::PROVIDER[omniauth[:provider]], omniauth[:uid])
		if authentication
			@user = authentication.user
			sign_in @user
			flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => omniauth[:provider]
			redirect_to user_path(@user)
		elsif current_user
			current_user.authentication.create(:uid => omniauth[:uid], :provider => Authentication::PROVIDER[omniauth[:provider]])
			flash[:notice] = "Authentication successfull."
			redirect_to user_path(@user)
		else
			@user = User.new(:email => omniauth[:info][:email], :first_name => omniauth[:info][:first_name], :last_name => omniauth[:info][:last_name])
			session[:omniauth] = omniauth.except('extra')
			render "users/registrations/new"
		end
	end
end