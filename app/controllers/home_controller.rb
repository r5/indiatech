class HomeController < ApplicationController
  def index
  	@featured = User.featured
  	@users = User.top_eight
  end
end
