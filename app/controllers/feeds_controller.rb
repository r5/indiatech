class FeedsController < ApplicationController

	before_filter :authenticate_user!, :only => [:feed]

	def feed
    @user = current_user
    get_feeds
  end

  def links
  	get_feeds("Link")
  end

  def cards
  	get_feeds("Card")
  end

  def micros
  	get_feeds("Micro")
  end

  def cards_links_micros
  	links
  	cards
  	micros
  end

  private

  def get_feeds(type=nil)
  	if !request.xhr?
      feeds = current_user.page_activities_oj(current_user.followed_pool.values, params[:page] || 1, 5, type)[0].html_safe
    else
      feeds, total_pages = current_user.page_activities_oj(current_user.followed_pool.values, params[:page] || 1, 5, type)
      feeds = feeds.html_safe
    end
    case type
    when "Link"
    	@link_feed = feeds
      @link_total_pages = total_pages
    when "Card"
    	@card_feed = feeds
      @card_total_pages = total_pages
    when "Micro"
    	@micro_feed = feeds
      @micro_total_pages = total_pages
    else
    	@feeds = feeds
      @total_pages = total_pages
    end   	
  end
end
