class CardsController < ApplicationController

	before_filter :authenticate_user!, :only => [:create, :update]

	before_filter :find_card, :only => [:show]
	
	def create
		@card = current_user.cards.build(params[:card])
		@card.save
	end

	def show
		if @card
			@user = @card.user
		else

		end
	end

private

	def find_card
		@card = Card.where(:slug => params[:id]).first
	end
end
