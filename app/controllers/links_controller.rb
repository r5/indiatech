class LinksController < ApplicationController

	before_filter :authenticate_user!, :only => [:create, :update]
	before_filter :find_link, :only => [:show]
	
	def create
		@link = current_user.links.build(params[:link])
		@link.save
	end


	def show
		if @link
			@user = @link.user
		else

		end
	end

private

	def find_link
		@link = Link.where(:slug => params[:id]).first
	end
end
