class NotificationsController < ApplicationController
	before_filter :authenticate_user!
	before_filter :find_notification, :only => [:mark_read, :destroy]

  def notifications
  	@notifications = 	current_user.notifications.paginate(:page => 1, :per_page => 30).includes([:actor])
  	render "index"
  end

  def mark_read
  	@notification.update_attribute("viewd_at", Time.zone.now)
  end

  def destroy
  	@notification.destroy
  end

  private

  def find_notification
  	@notification = Notification.where(params[:id]).first
  	render :status => 404 and return unless @notification
  end
end
