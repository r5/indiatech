class PhotosController < ApplicationController
	before_filter :authenticate_user!
	
	def create
		@photo = Photo.new(params[:photo])
		@photo.save
	end

	def destroy
		@photo = Photo.find(params[:id])
		@photo.destroy
	end

	def update
	end
end
