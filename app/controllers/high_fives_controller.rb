class HighFivesController < ApplicationController
	before_filter :authenticate_user!

	def create
    @high_five = HighFive.new(params[:high_five])
    if @high_five.save
      @high_five.resource.user.notify("#{params[:high_five][:resource_type].downcase}.highfived", @high_five.user, @high_five.resource )
    end
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @high_five = HighFive.find(params[:id])
    @high_five.destroy
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

end
