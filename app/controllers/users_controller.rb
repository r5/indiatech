class UsersController < ApplicationController

  before_filter :authenticate_user!, :only => [:edit,:share, :update,:add_company, :add_project, :dashboard, :lazy_dashboard, :feed]
	before_filter :find_user_by_username , :only => [:profile, :edit, :add_project,:add_company, :goodies]
  before_filter :find_username_or_authenticate, :only => [:followers, :followings]
  def profile
    @micros = @user.micros.paginate(:page => 1, :per_page => 10).includes(:high_fives)
    @links = @user.links.paginate(:page => 1, :per_page => 10).includes(:high_fives)
    @cards = @user.cards.paginate(:page => 1, :per_page => 10).includes(:high_fives)
  end

  def goodies
    if params[:type] == "micro"
      @micros = @user.micros.paginate(:page => params[:page] || 1, :per_page => 10).includes(:high_fives)
    elsif params[:type] == "link"
      @links = @user.links.paginate(:page =>  params[:page] || 1, :per_page => 10).includes(:high_fives)
    elsif params[:type] == "card"
      @cards = @user.cards.paginate(:page =>  params[:page] || 1, :per_page => 10).includes(:high_fives)
    end
  end

  def share
    @resource = params[:resource_type].constantize.where(:id => params[:track_id]).first
    if @resource
      @resource.activities.create({key: "#{params[:resource_type].downcase}.shared", user: current_user})
      @resource.user.notify("#{params[:resource_type].downcase}.shared", current_user, @resource)
    end
  end

  def dashboard
    @recent_notifications = current_user.notifications.unread.limit(10)
    @stats = {followers: current_user.followers_counter, followings: current_user.followed_counter}
  end

  def followers
    @followers = @user.followers.paginate(:per_page => 30, :page => params[:page] || 1).includes(:skills)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def followings
    @followings = @user.followed_users.paginate(:per_page => 30, :page => params[:page] || 1).includes(:skills)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def lazy_dashboard
    render :json => current_user.recent_activities_oj(current_user.follower_ids), :layout => false
  end

  def edit
    @user.schools.build if @user.schools.count == 0
    render 'edit_profile'
  end

  def add_project
    @user.projects.build if @user.projects.count == 0
    render 'update_project'
  end

  def add_company
    @user.company_user_profiles.build if @user.company_user_profiles.count == 0
    render 'update_company'
  end

  def update
  	current_user.update_attributes(params[:user])
  	redirect_to  user_root_path
  end

  protected
  	def find_user_by_username
  		@user = User.where(:username => params[:username]).first if params[:username]
  		redirect_to root_path, :error => "Could not find the user." unless @user
  	end

  	def user_is_current_user?
  		@user_is_current_user ||= (current_user == @user)
  	end

    def find_username_or_authenticate
      if params[:username]
        find_user_by_username
      else
        authenticate_user!
        @user = current_user
      end
    end

end
