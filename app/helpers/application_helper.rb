module ApplicationHelper

	def link_to_remove_fields(name, f)  
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)", :class => "remove close")  
  end

  def link_to_add_fields(name, f, association)  
	  new_object = f.object.class.reflect_on_association(association).klass.new  
	  fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|  
	    render(association.to_s.singularize + "_fields", :f => builder)  
	  end  
	  link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")".html_safe)  
	end  

	def more_or_less(text, limit, style=nil)
		return "" unless text
		#<a href='#' class='red' onclick='$(this).parent().toggle();$(this).parent().prev().toggle(); return false;'>less</a> 
		('<p class="less-text">' + truncate( text, :length => limit, :separator => ' ', :omission => "") + (text.length > limit ?  "&nbsp;&nbsp;<a style='cursor:pointer' class='red' onclick='$(this).parent().toggle();$(this).parent().next().toggle(); return false;'>more..</a></p> <p style='margin-top:0px;' class='more-text'>"  + text  + "</p>" : "</p>") ).html_safe
	end

	def reply_to(micro)
		micro.replied_post ? "@#{micro.user.username} @#{micro.replied_post.user.username} " : "@#{micro.user.username} "
	end

	def flash_type_class(type)
		type == :error ? "alert" : ""
	end

	def high_five_form(id, type, user_id)
		%{<form accept-charset="UTF-8" action="/high_fives" class="new_high_five" data-remote="true" id="new_high_five" method="post" style="margin: 0px">
      <input id="high_five_resource_id" name="high_five[resource_id]" type="hidden" value="#{id}">
      <input id="high_five_resource_type" name="high_five[resource_type]" type="hidden" value="#{type}">
      <input id="high_five_user_id" name="high_five[user_id]" type="hidden" value="#{user_id}">
      <input class="small button tag" data-disable-with = "wait" name="commit" type="submit" value="high five!">
    </form>}.html_safe
	end
end
