Indiatech::Application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "users/registrations", :sessions =>  'users/sessions'}

  get "home/index"

  resources :users, :only => [:update]

  get 'users/goodies/:type', as: :goodies, to: 'users#goodies'
  get 'dashboard', as: :user_root, to: 'users#dashboard'
  get 'lazy_dashboard', :as => :lazy_dashboard, to: 'users#lazy_dashboard'
  get 'feed', :as => :feed, to: 'feeds#feed'
  get 'feed/links', :as => :link_feed, to: 'feeds#links'
  get 'feed/cards', :as => :card_feed, to: 'feeds#cards'
  get 'feed/micros', :as => :micro_feed, to: 'feeds#micros'
  get 'feed/all', :as => :all_feed, to: 'feeds#cards_links_micros'
  get "users/share", :as => :share, to: 'users#share'

  get "notifications", :as => :notifications, to: 'notifications#notifications'
  resources :notifications, only: [:destroy] do
    member do
      post :mark_read
    end
  end
  resources :micros, only: [:create, :show]
  resources :cards, only: [:create, :show]
  resources :photos, :only => [:create, :update, :destroy]

  resources :relationships, only: [:create, :destroy]
  resources :high_fives, only: [:create, :destroy]
  resources :links, only: [:create, :destroy, :show]
  resources :link_categories, only: [:create, :destroy]

  get ':username/followers', :to => 'users#followers', :as => :user_followers
  get ':username/followings', :to => 'users#followers', :as => :user_followings
  get '/followers', :to => 'users#followers', :as => :followers
  get '/followings', :to => 'users#followings', :as => :followings
  get ':username/project/add', :to => 'users#add_project', :as => :user_add_project, :constraints => { :username => /[A-Za-z0-9_]+/} 
  get ':username/company/add', :to => 'users#add_company', :as => :user_add_company, :constraints => { :username => /[A-Za-z0-9_]+/}

  get ':username', :to => 'users#profile', :as => :user_profile, :constraints => lambda{|req| 
                                                                                         req[:username].match(/[A-Za-z0-9_]+/) && req[:username] != 'dashboard' && req[:username] != "lazy_dashboard" && req[:username] != "feed"
                                                                                       }
  get ':username/edit', :to => 'users#edit', :as => :user_edit, :constraints =>  lambda{|req| 
                                                                                         req[:username].match(/[A-Za-z0-9_]+/) && req[:username] != 'dashboard' && req[:username] != "lazy_dashboard" && req[:username] != "feed"
                                                                                       }


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
