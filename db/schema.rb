# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130302194522) do

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "user_id"
    t.string   "key"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "activities", ["user_id", "trackable_type"], :name => "by_trackable_and_user"
  add_index "activities", ["user_id"], :name => "by_user"

  create_table "authentications", :force => true do |t|
    t.string   "uid"
    t.integer  "provider"
    t.string   "access_token"
    t.string   "secret"
    t.string   "image"
    t.string   "profile"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "user_id"
  end

  create_table "cards", :force => true do |t|
    t.text     "description"
    t.integer  "view_count"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
    t.text     "detail"
    t.string   "link"
  end

  add_index "cards", ["slug"], :name => "index_cards_on_slug"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "website"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "company_user_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "title"
    t.string   "location"
    t.text     "technologies_clone"
    t.datetime "work_from"
    t.datetime "work_till"
    t.boolean  "working_currently"
    t.text     "summary"
    t.string   "company_name"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "high_fives", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "resource_type"
    t.integer  "resource_id"
  end

  create_table "link_categories", :force => true do |t|
    t.string   "title"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "links", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.integer  "link_category_id"
    t.string   "image_url"
    t.integer  "vote_ups"
    t.string   "link"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "view_count",       :default => 0
    t.string   "slug"
  end

  add_index "links", ["slug"], :name => "index_links_on_slug"

  create_table "micros", :force => true do |t|
    t.integer  "user_id"
    t.string   "content"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "in_reply_to"
    t.integer  "view_count",  :default => 0
    t.string   "slug"
  end

  add_index "micros", ["slug"], :name => "index_micros_on_slug"

  create_table "notifications", :force => true do |t|
    t.integer  "user_id"
    t.datetime "viewed_at"
    t.datetime "closed_at"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.string   "key"
    t.integer  "position"
    t.integer  "actor_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "photos", :force => true do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  create_table "projects", :force => true do |t|
    t.string   "name"
    t.string   "repo_url"
    t.string   "site_url"
    t.string   "status"
    t.datetime "from"
    t.datetime "to"
    t.integer  "user_id"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "relationships", :force => true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "relationships", ["followed_id"], :name => "index_relationships_on_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], :name => "index_relationships_on_follower_id_and_followed_id", :unique => true
  add_index "relationships", ["follower_id"], :name => "index_relationships_on_follower_id"

  create_table "schools", :force => true do |t|
    t.string   "name"
    t.string   "degree"
    t.string   "fileds"
    t.datetime "from"
    t.datetime "to"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "social_links", :force => true do |t|
    t.string   "link"
    t.string   "link_type"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.text     "bio"
    t.string   "title"
    t.integer  "company_name"
    t.string   "location"
    t.string   "teaser"
    t.integer  "status",                                :default => 0
    t.string   "username"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "job_availability"
    t.boolean  "freelancer"
    t.string   "languages"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
