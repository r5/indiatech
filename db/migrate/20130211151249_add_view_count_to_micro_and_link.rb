class AddViewCountToMicroAndLink < ActiveRecord::Migration
  def change
    add_column :links, :view_count, :integer, :default => 0
    add_column :micros, :view_count, :integer, :default => 0
  end
end
