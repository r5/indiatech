class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.string :uid
      t.integer :provider
      t.string :access_token
      t.string :secret
      t.string :image
      t.string :profile
      t.timestamps
    end
  end
end
