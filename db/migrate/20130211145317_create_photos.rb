class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
    	t.string :attachable_type
      t.integer :attachable_id

      t.timestamps
    end
  end
end
