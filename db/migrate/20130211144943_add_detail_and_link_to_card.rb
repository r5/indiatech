class AddDetailAndLinkToCard < ActiveRecord::Migration
  def change
    add_column :cards, :detail, :text
    add_column :cards, :link, :string
  end
end
