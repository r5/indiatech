class AddMicroIdToHighFive < ActiveRecord::Migration
  def change
    add_column :high_fives, :micro_id, :integer
  end
end
