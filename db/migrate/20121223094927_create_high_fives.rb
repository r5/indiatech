class CreateHighFives < ActiveRecord::Migration
  def change
    create_table :high_fives do |t|
      t.integer :card_id
      t.integer :user_id

      t.timestamps
    end
  end
end
