class AddInReplyToToMicro < ActiveRecord::Migration
  def change
    add_column :micros, :in_reply_to, :integer
  end
end
