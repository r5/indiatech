class AddFirstNameAndLastNameAndBioTitleAndCompanyIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :bio, :text
    add_column :users, :title, :string
    add_column :users, :company_id, :integer
    add_column :users, :location, :string
    add_column :users, :teaser, :string
    add_column :users, :status, :integer, :default => 0
  end
end
