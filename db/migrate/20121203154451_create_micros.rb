class CreateMicros < ActiveRecord::Migration
  def change
    create_table :micros do |t|
      t.integer :user_id
      t.string :content

      t.timestamps
    end
  end
end
