class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.datetime :viewed_at
      t.datetime :closed_at
      t.integer :resource_id
      t.string :resource_type
      t.string :key
      t.integer :position
      t.integer :actor_id

      t.timestamps
    end
  end
end
