class AddLinkIdToHighFive < ActiveRecord::Migration
  def change
    add_column :high_fives, :link_id, :integer
  end
end
