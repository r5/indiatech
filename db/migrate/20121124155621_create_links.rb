class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :title
      t.text :description
      t.integer :user_id
      t.integer :link_category_id
      t.string :image_url
      t.integer :vote_ups
      t.string :link

      t.timestamps
    end
  end
end
