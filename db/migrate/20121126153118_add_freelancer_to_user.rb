class AddFreelancerToUser < ActiveRecord::Migration
  def change
    add_column :users, :freelancer, :boolean
  end
end
