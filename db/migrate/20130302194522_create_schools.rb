class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.string :degree
      t.string :fileds
      t.datetime :from
      t.datetime :to
      t.integer :user_id

      t.timestamps
    end
  end
end
