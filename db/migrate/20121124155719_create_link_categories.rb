class CreateLinkCategories < ActiveRecord::Migration
  def change
    create_table :link_categories do |t|
      t.string :title
      t.integer :user_id

      t.timestamps
    end
  end
end
