class AddActivityIndexes < ActiveRecord::Migration
  def up
  	add_index :activities, :user_id, :name => :by_user
  	add_index :activities, [:user_id, :trackable_type], :name => :by_trackable_and_user
  end

  def down
  	remove_index :activities, :name => :by_user
  	add_index :activities, :name => :by_trackable_and_user
  end
end
