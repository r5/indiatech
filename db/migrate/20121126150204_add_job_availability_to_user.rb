class AddJobAvailabilityToUser < ActiveRecord::Migration
  def change
    add_column :users, :job_availability, :integer
  end
end
