class AddSlugToMicro < ActiveRecord::Migration
  def change
    add_column :micros, :slug, :string
    add_index :micros, :slug
  end
end
