class AddResourceTypeAndResourceIdToHighFive < ActiveRecord::Migration
  def change
    add_column :high_fives, :resource_type, :string
    add_column :high_fives, :resource_id, :integer
    remove_column :high_fives, :card_id
    remove_column :high_fives, :link_id
    remove_column :high_fives, :micro_id
  end
end
