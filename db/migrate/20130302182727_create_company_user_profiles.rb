class CreateCompanyUserProfiles < ActiveRecord::Migration
  def change
    create_table :company_user_profiles do |t|
      t.integer :user_id
      t.integer :company_id
      t.string :title
      t.string :location
      t.text :technologies_clone
      t.datetime :work_from
      t.datetime :work_till
      t.boolean :working_currently
      t.text :summary
      t.string :company_name

      t.timestamps
    end
  end
end
