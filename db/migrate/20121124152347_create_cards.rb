class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.text :description
      t.integer :view_count
      t.integer :user_id

      t.timestamps
    end
  end
end
