class CreateSocialLinks < ActiveRecord::Migration
  def change
    create_table :social_links do |t|
      t.string :link
      t.string :link_type
      t.integer :user_id

      t.timestamps
    end
  end
end
