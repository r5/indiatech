class RemovePublicActivityGem < ActiveRecord::Migration
  def up
  	remove_column :activities, :owner_type
  	rename_column :activities, :owner_id, :user_id
  	remove_column :activities, :parameters
  end

  def down
  	add_column :activities, :owner_type, :string
  	rename_column :activities, :user_id, :owner_id
  	add_column :activities, :parameters, :text
  end
end
