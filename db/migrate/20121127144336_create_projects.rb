class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :repo_url
      t.string :site_url
      t.string :status
      t.datetime :from
      t.datetime :to
      t.integer :user_id
      t.text :description

      t.timestamps
    end
  end
end
