class RenameCompanyIdToCompanyName < ActiveRecord::Migration
  def up
  	rename_column :users, :company_id, :company_name
  end

  def down
  	rename_column :users, :company_name, :company_id
  end
end
